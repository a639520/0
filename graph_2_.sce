clc
clf
x=[0:0.02:4*%pi]
subplot(2,2,1)
plot2d(x,sin(x),3)
xtitle("Graph between x and sin(x)")
xlabel("x")
ylabel("sin(x)")
