clc
clf
L=4
x=0:0.02:4
h=6.6*10^-34
m=9.11*10^-31
for(n=1:1:5)
p=sqrt(2/L)*sin((n*%pi*x)/L)
e(n)=n^2*%pi^2*h^2/(8*m*L^2)
k=p^2
disp(p)
disp(e(n))
disp(k)
subplot(5,1,n)
plot2d(x,p,2)
subplot(5,1,n)
plot2d(x,K,4)
end
legend('wavefunction','probability density')
legend(4,"probability density")
legend(2,"wavefunction")
xlabel("x")    
